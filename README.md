# Dockerimages for php development

## Build image

Build basic

    docker build --pull --target base --build-arg="PHP_VERSION=8.2" -t php-apache:8.2 .

## Test image

    docker run -it --rm php-apache:8.2 bash