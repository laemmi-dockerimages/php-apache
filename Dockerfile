ARG PHP_VERSION=8.1

FROM php:${PHP_VERSION}-apache AS base

WORKDIR /tmp

ADD /apache /

# Copy scripts
COPY --chmod=755 ./scripts /tmp/

# Install scripts
RUN ./composer.sh \
#    && ./deployer7.sh \
    && ./php_ini.sh \
#
# Packages
    && apt-get update \
    && apt-get install -y --no-install-recommends \
    ca-certificates \
    curl \
    git \
    less \
    libc-client-dev \
    libfreetype-dev \
    libicu-dev \
    libjpeg-dev \
    libonig-dev \
    libpng-dev \
    libwebp-dev \
    libxpm-dev \
    libssl-dev \
    libxslt-dev \
    libzip-dev \
    nano \
    openssh-client \
    openssl \
    rsync \
    unzip \
    vim \
    zip \
#
### PHP extensions configure
&& docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-xpm \
#
### PHP extensions install
&& docker-php-ext-install -j "$(nproc)" \
    bcmath \
    exif \
    gd \
    intl \
    pdo \
    pdo_mysql \
    mbstring \
    mysqli \
    opcache \
    soap \
    sockets \
    xml \
    xsl \
    zip \
    && docker-php-source delete \
#
### PHP pecl extensions install
&& pecl install \
    redis \
#
### PHP extensions enable
&& docker-php-ext-enable \
    gd \
    intl \
    mbstring \
    redis \
#
### Apache modules
&& a2enmod headers \
    && a2enmod rewrite \
    && a2enmod expires \
    && a2enmod proxy \
    && a2enmod proxy_http \
    && a2enmod proxy_wstunnel \
    && a2enmod actions \
#    && a2enmod fcgid \
    && a2enmod alias \
    && a2enmod proxy_fcgi \
#
## Cleanup
&& ./cleanup.sh

WORKDIR /var/www/html