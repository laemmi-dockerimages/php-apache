#!/bin/bash

set -euf -o pipefail

cp $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini
sed -i "s/memory_limit = .*M/memory_limit = -1/" $PHP_INI_DIR/php.ini
sed -i "s/max_execution_time = .*/max_execution_time = 300/" $PHP_INI_DIR/php.ini
sed -i "s/variables_order = .*/variables_order = EGPCS/" $PHP_INI_DIR/php.ini
